import datetime
from pathlib import Path
from typing import Optional

import enchant
import numpy as np
import pandas as pd
from catboost import CatBoostRegressor
from sklearn.model_selection import train_test_split


class FeatureExtractor:
    ''' Extracts features from Password column
        You can provide your own extractor with the same interface to
        PasswordAssessor class
    '''
    def __init__(self):
        self._vocab = enchant.Dict("en_US")

    def extract_features(self, data: pd.DataFrame) -> pd.DataFrame:
        data['isalpha'] = data['Password'].apply(lambda x: str(x).isalpha())
        data['isdigit'] = data['Password'].apply(lambda x: str(x).isdigit())
        data['isalnum'] = data['Password'].apply(lambda x: str(x).isalnum())
        data['length'] = data['Password'].apply(lambda x: len(str(x)))
        data['distinct_chars'] = data['Password'].apply(
            lambda x: len(set(str(x)))
            )
        data['variability'] = data['distinct_chars'] / data['length']
        data['upper_case'] = data['Password'].apply(
            lambda x: str(x) == str(x).upper()
            )
        data['lower_case'] = data['Password'].apply(
            lambda x: str(x) == str(x).lower()
            )

        data['is_ddmmyyyy'] = data['Password'].apply(
            FeatureExtractor.is_date_ddmmyyyy
            )
        data['ddmmyyyy_year'] = 0
        data.loc[data['is_ddmmyyyy'],
                 'ddmmyyyy_year'
                 ] = data.loc[data['is_ddmmyyyy'], 'Password'].apply(
                     lambda x: datetime.datetime.strptime(x, '%d%m%Y').year
                     )

        data['is_ddmmyy'] = data['Password'].apply(
            FeatureExtractor.is_date_ddmmyy
            )
        data['ddmmyy_year'] = 0
        data.loc[data['is_ddmmyy'],
                 'ddmmyy_year'
                 ] = data.loc[data['is_ddmmyy'], 'Password'].apply(
                     FeatureExtractor.get_year_ddmmyy
                     )

        data['is_mmddyyyy'] = data['Password'].apply(
            FeatureExtractor.is_date_mmddyyyy
            )
        data['mmddyyyy_year'] = 0
        data.loc[data['is_mmddyyyy'],
                 'mmddyyyy_year'
                 ] = data.loc[data['is_mmddyyyy'], 'Password'].apply(
                     FeatureExtractor.get_year_mmddyyyy
                     )

        data['is_mmddyy'] = data['Password'].apply(
            FeatureExtractor.is_date_mmddyy
            )
        data['mmddyy_year'] = 0
        data.loc[data['is_mmddyy'],
                 'mmddyy_year'
                 ] = data.loc[data['is_mmddyy'], 'Password'].apply(
                     FeatureExtractor.get_year_mmddyy
                     )

        data['is_word'] = False
        data.loc[(data['isalpha']) & (~data['Password'].isna()),
                 'is_word'] = data.loc[(data['isalpha'])
                                       & (~data['Password'].isna()),
                                       'Password'].apply(self.is_word)
        return data

    @staticmethod
    def is_date_ddmmyyyy(password):
        if len(str(password)) != 8:
            return False
        try:
            date = datetime.datetime.strptime(password, '%d%m%Y')
            if date.year < 1900 or date.year > 2021:
                return False
            return True
        except Exception:
            return False

    @staticmethod
    def is_date_ddmmyy(password):
        if len(str(password)) != 6:
            return False
        try:
            datetime.datetime.strptime(password, '%d%m%y')
            return True
        except Exception:
            return False

    @staticmethod
    def get_year_ddmmyy(password):
        year = int(password[-2:])
        if year < 22:
            return year + 2000
        else:
            return year + 1900

    @staticmethod
    def is_date_mmddyyyy(password):
        if len(str(password)) != 8:
            return False
        try:
            date = datetime.datetime.strptime(password, '%m%d%Y')
            if date.year < 1900 or date.year > 2021:
                return False
            return True
        except Exception:
            return False

    @staticmethod
    def get_year_mmddyyyy(password):
        year = int(password[-4:])
        return year

    @staticmethod
    def is_date_mmddyy(password):
        if len(str(password)) != 6:
            return False
        try:
            datetime.datetime.strptime(password, '%m%d%y')
            return True
        except Exception:
            return False

    @staticmethod
    def get_year_mmddyy(password):
        year = int(password[-2:])
        if year < 22:
            return year + 2000
        else:
            return year + 1900

    def is_word(self, s):
        return self._vocab.check(s) or self._vocab.check(s.capitalize())


class PasswordAssessor:
    def __init__(self, feature_generator: Optional[FeatureExtractor] = None):
        self._model: CatBoostRegressor = CatBoostRegressor()
        self._feature_extractor = feature_generator
        if feature_generator is None:
            self._feature_extractor = FeatureExtractor()

    def load_model(self, model_path: Path) -> None:
        self._model.load_model(model_path)

    def save_model(self, model_path: Path) -> None:
        self._model.save_model(model_path)

    def fit(self, data: pd.DataFrame, test_size: float = 0.1):
        ''' data - a DataFrame with Password and Times columns
            Method uses feature extractor to extract features and then
            train CatBoostRegressor.
        '''
        data_with_feats = self._feature_extractor.extract_features(data)
        X = data_with_feats.drop(['Password', 'Times'], axis=1).values
        y = np.log(data_with_feats['Times'].values + 1)
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=test_size, random_state=42
            )
        self._model.fit(X_train, y_train, eval_set=(X_test, y_test))

    def predict(self, password: str) -> float:
        df = pd.DataFrame.from_records({'Password': [password]})
        feats = self._feature_extractor.extract_features(df).drop('Password',
                                                                  axis=1)
        prediction = np.exp(self._model.predict(feats.values)[0]) - 1
        return {'password': password,
                'prediction': prediction}
