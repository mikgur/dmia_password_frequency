FROM python:3.8-slim

ENV USER=apps
ENV UID=1002
ENV GID=1002
# ENV FLASK_APP=password_app

RUN addgroup --gid "$GID" "$USER" \
    && adduser \
    --disabled-password \
    --gecos "" \
    --ingroup "$USER" \
    --uid "$UID" \
    "$USER"

RUN pip install pipenv

RUN apt-get update \
    && apt-get install gcc -y \
    && apt-get install -y libenchant-dev \
    && apt-get clean

WORKDIR /app

# Create the environment:
COPY Pipfile .
COPY Pipfile.lock .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy
ENV PATH="/app/.venv/bin:$PATH"

USER $USER

# The code to run when container is started:
COPY --chown=apps . /app/

CMD gunicorn -b 0.0.0.0:8000 "password_app:create_app()"