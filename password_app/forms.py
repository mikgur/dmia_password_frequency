from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class PasswordForm(FlaskForm):
    password = StringField("Пароль: ", validators=[DataRequired()],
                           render_kw={"class": "form-control"})
    submit = SubmitField("Оценить", render_kw={"class": "btn btn-primary"})
