from config import MODEL_PATH, SECRET_KEY
from flask import Flask, render_template
from model import PasswordAssessor

from password_app.forms import PasswordForm


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = SECRET_KEY

    assessor = PasswordAssessor()
    assessor.load_model(MODEL_PATH)

    @app.route("/", methods=["GET"])
    def index():
        form = PasswordForm()
        return render_template("main.html", form=form,
                               password="",
                               prediction="")

    @app.route("/predict_frequency", methods=["POST"])
    def predict_frequency():
        form = PasswordForm()

        if form.validate_on_submit():
            password = form.password.data
            result = assessor.predict(password)
            return render_template("main.html", form=form,
                                   password=result['password'],
                                   prediction=f'{result["prediction"]:0.2f}')
        return render_template("main.html", form=form)

    return app


app = create_app()
